import Router from "vue-router";
import auth from "../services/auth";
import DashView from "../components/Dash.vue";
import LoginView from "../views/auth/Login.vue";
import NotFoundView from "../components/404.vue";

// POST
import Post from "../views/post/index";
import PostEdit from "../views/post/edit/index";
import PostAdd from "../views/post/add/index";
import PostShow from "../views/post/show/index";

// Check Post
import CheckPost from "../views/check_post/index";

// CATEGORY
import Category from "../views/category/index";
import CategoryEdit from "../views/category/edit";
import CategoryAdd from "../views/category/add";

// USER
import User from "../views/user/index";
import UserAdd from "../views/user/add";
import UserEdit from "../views/user/edit";

// NEWS
import News from "../views/news/index";
import NewsAdd from "../views/news/add";
import NewsEdit from "../views/news/edit";

// AGENCY
import Agency from "../views/agency/index";
import AgencyAdd from "../views/agency/add";
import AgencyEdit from "../views/agency/edit";

// GET PRICE LIST
import GetPriceList from "../views/get_price_list/index";

// ADDITIONAL QUESTION
import AdditionalQuestion from "../views/additional_question/index";

// WATCH LIVE
import WatchLive from "../views/watch_live/index";

import Vue from "vue";

Vue.use(Router);

// Routes
const router = new Router({
  linkExactActiveClass: "active", // link active
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/login",
      component: LoginView
    },
    {
      path: "/",
      component: DashView,
      children: [
        // POST
        {
          path: "/post",
          component: Post,
          name: "post.index",
          meta: { requiresAuth: true }
        },
        {
          path: "/post/show/:id",
          component: PostShow,
          name: "post.show",
          meta: { requiresAuth: true }
        },
        {
          path: "/post/add",
          component: PostAdd,
          name: "post.add",
          meta: { requiresAuth: true }
        },
        {
          path: "/post/edit/:id",
          component: PostEdit,
          name: "post.edit",
          meta: { requiresAuth: true }
        },
        // Check Post
        {
          path: "/checkPost",
          component: CheckPost,
          name: "checkPost.index",
          meta: { requiresAuth: true }
        },
        // CATEGORY
        {
          path: "/category",
          component: Category,
          name: "category.index",
          meta: { requiresAuth: true }
        },
        {
          path: "/category/add",
          component: CategoryAdd,
          name: "category.add",
          meta: { requiresAuth: true }
        },
        {
          path: "/category/edit/:id",
          component: CategoryEdit,
          name: "category.edit",
          meta: { requiresAuth: true }
        },
        // USER
        {
          path: "/user",
          component: User,
          name: "user.index",
          meta: { requiresAuth: true }
        },
        {
          path: "/user/add",
          component: UserAdd,
          name: "user.add",
          meta: { requiresAuth: true }
        },
        {
          path: "/user/edit/:id",
          component: UserEdit,
          name: "user.edit",
          meta: { requiresAuth: true }
        },
        // NEWS
        {
          path: "/news",
          component: News,
          name: "news.index",
          meta: { requiresAuth: true }
        },
        {
          path: "/news/add",
          component: NewsAdd,
          name: "news.add",
          meta: { requiresAuth: true }
        },
        {
          path: "/news/edit/:id",
          component: NewsEdit,
          name: "news.edit",
          meta: { requiresAuth: true }
        },
        // AGENCY
        {
          path: "/agency",
          component: Agency,
          name: "agency.index",
          meta: { requiresAuth: true }
        },
        {
          path: "/agency/add",
          component: AgencyAdd,
          name: "agency.add",
          meta: { requiresAuth: true }
        },
        {
          path: "/agency/edit/:id",
          component: AgencyEdit,
          name: "agency.edit",
          meta: { requiresAuth: true }
        },
        // GET PRICE LIST
        {
          path: "/getPriceList",
          component: GetPriceList,
          name: "getPriceList.index",
          meta: { requiresAuth: true }
        },
        // Additional Question
        {
          path: "/additionalQuestion",
          component: AdditionalQuestion,
          name: "additionalQuestion.index",
          meta: { requiresAuth: true }
        },
        // Watch Live
        {
          path: "/watchLive",
          component: WatchLive,
          name: "watchLive.index",
          meta: { requiresAuth: true }
        }
      ]
    },
    {
      // not found handler
      path: "*",
      component: NotFoundView
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!auth.isLoggedIn()) {
      next({
        path: "/login",
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});

export default router;
