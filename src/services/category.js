import axios from "axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "api/category/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("api/category/create", data);
}

function show(id) {
  return axios.get("api/category/show/" + id);
}

function update(id, data) {
  return axios.post("api/category/update/" + id, data);
}

function remove(id) {
  return axios.delete("api/category/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "api/category/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

function upload(formData, config) {
  return axios.post("api/category/upload", formData, config);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  getSearch,
  upload
};
