import axios from "axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "api/agency/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("api/agency/create", data);
}

function show(id) {
  return axios.get("api/agency/show/" + id);
}

function update(id, data) {
  return axios.post("api/agency/update/" + id, data);
}

function remove(id) {
  return axios.delete("api/agency/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "api/agency/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

function upload(formData, config) {
  return axios.post("api/agency/upload", formData, config);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  getSearch,
  upload
};
