import axios from "axios";
/**
 * Tạo user mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll() {
  return axios.get("api/user/list");
}

function getPaginate(page, perPage, sortColumn, direction) {
  return axios.get(
    "api/user/getPaginate?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("api/user/create", data);
}

function show(id) {
  return axios.get("api/user/show/" + id);
}

function update(id, data) {
  return axios.post("api/user/update/" + id, data);
}

function remove(id) {
  return axios.delete("api/user/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "api/user/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  show,
  update,
  getPaginate,
  submit,
  remove,
  getSearch
};
