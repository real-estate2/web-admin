import axios from "axios";

function isLoggedIn() {
  return localStorage.getItem("isLoggedIn");
}

function isLoggedOut() {
  this.removeToken();
  return axios.post("/logout");
}

export function removeToken() {
  localStorage.removeItem("isLoggedIn");
  localStorage.removeItem("isUsername");
}

export default {
  isLoggedIn,
  isLoggedOut,
  removeToken
};
