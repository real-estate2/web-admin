import axios from "axios";

/**
 * Tải lên tập tin hình ảnh vào thư mục tạm trên CDN
 *
 * @param file
 * @param onUploadProgress
 * @returns {Promise<AxiosResponse<T>>}
 */
function uploadFile(file, onUploadProgress) {
  let formData = new FormData();
  formData.append("file", file);

  return axios.post(`api/image.upload`, formData, {
    onUploadProgress: onUploadProgress
  });
}

export default {
  uploadFile
};
