import axios from "axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "api/news/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("api/news/create", data);
}

function show(id) {
  return axios.get("api/news/show/" + id);
}

function update(id, data) {
  return axios.post("api/news/update/" + id, data);
}

function remove(id) {
  return axios.delete("api/news/remove/" + id);
}

function undoRemove(id) {
  return axios.delete("api/news/undoRemove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "api/news/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

function upload(formData, config) {
  return axios.post("api/news/upload", formData, config);
}

export default {
  getAll,
  submit,
  show,
  update,
  remove,
  undoRemove,
  getSearch,
  upload
};
