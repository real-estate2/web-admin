import axios from "axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "api/post/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function submit(data) {
  return axios.post("api/post/create", data);
}

function show(id) {
  return axios.get("api/post/show/" + id);
}

function update(id, data) {
  return axios.post("api/post/update/" + id, data);
}

function remove(id) {
  return axios.delete("api/post/remove/" + id);
}

function undoRemove(id) {
  return axios.delete("api/post/undoRemove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyPost) {
  return axios.post(
    "api/post/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyPost
  );
}

export default {
  // getOptions,
  getAll,
  submit,
  show,
  update,
  remove,
  undoRemove,
  getSearch
};
