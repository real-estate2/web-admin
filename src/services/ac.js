import axios from "axios";

function getProvinces() {
  return axios.get("api/province.list");
}

function getProvince(id) {
  return axios.get("api/province.info/" + id);
}

function getDistricts(id) {
  return axios.get("api/province.getDistricts/" + id);
}

function getDistrictBundle(id) {
  return axios.get("api/district.bundle/" + id);
}

function getDistrictGeoData(id) {
  return axios.get("api/district.geoData/" + id);
}

export default {
  getProvinces,
  getProvince,
  getDistricts,

  getDistrictBundle,
  getDistrictGeoData
};
