import axios from "axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function getAll(page, perPage, sortColumn, direction) {
  return axios.get(
    "api/checkPost/list?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction
  );
}

function show(id) {
  return axios.get("api/checkPost/show/" + id);
}

function update(id, data) {
  return axios.post("api/checkPost/checked/" + id, data);
}

function remove(id) {
  return axios.delete("api/checkPost/remove/" + id);
}

function getSearch(page, perPage, sortColumn, direction, keyText) {
  return axios.post(
    "api/checkPost/searchAll?page=" +
      page +
      "&per_page=" +
      perPage +
      "&sort_column= " +
      sortColumn +
      "&direction=" +
      direction,
    keyText
  );
}

export default {
  getAll,
  show,
  update,
  remove,
  getSearch
};
