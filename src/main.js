import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import Axios from "axios";
import Paginate from "vuejs-paginate";
Vue.use(Paginate);

import CKEditor from "@ckeditor/ckeditor5-vue";
Vue.use(CKEditor);

import { LMap, LTileLayer, LMarker } from "vue2-leaflet";
import { Icon } from "leaflet";

Vue.component("l-map", LMap);
Vue.component("l-tile-layer", LTileLayer);
Vue.component("l-marker", LMarker);

delete Icon.Default.prototype._getIconUrl;

Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png")
});

Axios.defaults.withCredentials = true;
Axios.defaults.baseURL = "http://app.airlock-example.test:8000/"; // Đường dẫn đầu tiên + service api
// Axios.defaults.baseURL = "http://192.168.1.5:8000/api/"; // Đường dẫn đầu tiên + service api

Vue.config.productionTip = false;

new Vue({
  el: "#root",
  router,
  store,
  render: h => h(App)
}).$mount("#app");
