import Vue from "vue";
import Vuex from "vuex";

import PostAddModule from "../store/modules/post_add.js";
import PostEditModule from "../store/modules/post_edit.js";
import PostAcModule from "./modules/post_ac.js";

import CategoryModule from "./modules/category_add.js";
import CategoryAcModule from "./modules/category_ac.js";

import UserAddModule from "../store/modules/user_add.js";

import NewsAddModule from "../store/modules/news_add.js";

import AgencyAddModule from "../store/modules/agency_add.js";

// OPTIONS

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    post_add: PostAddModule,
    post_edit: PostEditModule,
    post_ac: PostAcModule,

    category_add: CategoryModule,
    category_ac: CategoryAcModule,

    user_add: UserAddModule,

    news_add: NewsAddModule,

    agency_add: AgencyAddModule

    // OPTIONS
  }
});
