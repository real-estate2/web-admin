import AcService from "../../services/ac";

const CategoryModule = {
  namespaced: true,
  state: {
    isLoading: false,

    provinceOptions: [],
    districtOptions: [],

    polygon: [],
    latitude: null,
    longitude: null
  },
  actions: {
    loadProvinceData({ commit }) {
      commit("onLoadProvinceData");
      return new Promise((resolve, reject) => {
        AcService.getProvinces()
          .then(resp => {
            commit("onProcessingProvinceData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingProvinceDataFailure");
            reject(err);
          });
      });
    },

    changeProvince({ commit }, provinceId) {
      return new Promise((resolve, reject) => {
        AcService.getDistricts(provinceId)
          .then(resp => {
            commit("onProcessingDistrictData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadProvinceData(state) {
      state.isLoading = true;
    },
    onProcessingProvinceData(state, data) {
      state.isLoading = false;

      state.provinceOptions = [
        { value: 0, text: "--- Chọn tỉnh thành phố ---" }
      ];
      state.districtOptions = [{ value: 0, text: "--- Chọn quận huyện ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.provinceOptions.push({
          value: data[i]["province_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingProvinceDataFailure(state) {
      state.isLoading = false;
    },

    onProcessingDistrictData(state, data) {
      state.districts = data;

      state.districtOptions = [{ value: 0, text: "--- Chọn quận huyện ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.districtOptions.push({
          value: data[i]["district_id"],
          text: data[i]["pre"] + " " + data[i]["name"]
        });
      }
    }
  }
};

export default CategoryModule;
