import UserService from "../../services/user";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    username: "",
    password: "",
    last_name: "",
    first_name: "",
    email: "",
    mobile: "",
    sex: ""
  }
};

const UserAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        username: state.basicInfo.username,
        password: state.basicInfo.password,
        last_name: state.basicInfo.last_name,
        first_name: state.basicInfo.first_name,
        email: state.basicInfo.email,
        mobile: state.basicInfo.mobile,
        sex: state.basicInfo.sex
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        UserService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state, data) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.username = basicInfo.username;
      state.basicInfo.password = basicInfo.password;
      state.basicInfo.last_name = basicInfo.last_name;
      state.basicInfo.first_name = basicInfo.first_name;
      state.basicInfo.email = basicInfo.email;
      state.basicInfo.mobile = basicInfo.mobile;
      state.basicInfo.sex = basicInfo.sex;
    },

    reset(state) {
      state.basicInfo.username = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default UserAddModule;
