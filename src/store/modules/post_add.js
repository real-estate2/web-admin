import PostService from "../../services/post";
import { getPriceForPostType } from "../../util/pricing";
import moment from "moment";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    title: "",
    price: "",
    price_unit: "",
    price_total: "",
    area: "",
    area_unit: "",
    address: "",

    has_image: "",
    has_image_360: "",

    author: "",
    approved_by: "",
    approved_at: "",
    rejected_by: "",
    rejected_at: "",

    province_id: "",
    district_id: "",
    ward_id: "",
    street_id: "",

    product_category: null,
    project_id: "",
    product_type: ""
  },
  description: "",
  otherInfo: {
    main_direction: 0,
    nbr_bed_room: 0,
    nbr_rest_room: 0,
    nbr_floor: 0,
    front_street_width: 0,
    behind_street_width: 0
  },
  contactInfo: {
    contact_name: "",
    contact_address: "",
    contact_mobile: "",
    contact_email: ""
  },
  mediaInfo: {
    files: [],
    preview: [],
    maps: {
      longitude: 0,
      latitude: 0,
      zoom: 15
    },
    thumbnails: "",
    youtube: ""
  },
  post_type: 0,

  pricing: {
    per_day: 0,
    subtotal: 0,
    tax_amount: 0,
    grand_total: 0
  },

  scheduleInfo: {
    start_date: new Date(new Date().getTime() + 86400000)
      .toISOString()
      .slice(0, 10),
    end_date: new Date(new Date().getTime() + 15 * 86400000)
      .toISOString()
      .slice(0, 10),
    numberOfDay: 14
  }
};

const PostAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    /**
     * Cập nhật mô tả
     *
     * @param commit
     * @param basicInfo
     */
    setDescription({ commit }, description) {
      commit("setDescription", description);
    },

    /**
     * Cập nhật thông tin khác
     *
     * @param commit
     * @param otherInfo
     */
    setOtherInfo({ commit }, otherInfo) {
      commit("setOtherInfo", otherInfo);
    },

    /**
     * Cập nhật thông tin bản đồ
     *
     * @param commit
     * @param mapInfo
     */
    setMapInfo({ commit }, mapInfo) {
      commit("setMapInfo", mapInfo);
    },

    /**
     * Cập nhật thông tin liên hệ
     *
     * @param commit
     * @param contact
     */
    setContactInfo({ commit }, contact) {
      commit("setContactInfo", contact);
    },

    /**
     * Thay đổi loại tin
     *
     * @param commit
     * @param type
     */
    setPostType({ commit }, type) {
      commit("setPostType", type);
      commit("calculatePrice");
    },

    /**
     * Cập nhật lịch trình hiển thị tin
     *
     * @param commit
     * @param schedule
     */
    setSchedule({ commit }, schedule) {
      commit("setSchedule", schedule);
      commit("calculatePrice");
    },

    /**
     * Thêm hình ảnh đại diện cho bài đăng
     *
     * @param commit
     * @param url
     */
    setThumbnails({ commit }, url) {
      commit("setThumbnails", url);
    },

    /**
     * Thêm hình ảnh vào bài đăng nháp
     *
     * @param commit
     * @param image
     */
    addImage({ commit }, image) {
      commit("addImage", image);
    },

    /**
     * Bỏ hình ảnh khỏi bài đăng nháp
     *
     * @param commit
     * @param index
     */
    removeImage({ commit }, index) {
      commit("removeImage", index);
    },

    /**
     * Thêm link youtube vào bài đăng
     *
     * @param commit
     * @param url
     */
    setLinkYoutube({ commit }, url) {
      commit("setLinkYoutube", url);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        title: state.basicInfo.title,
        price: state.basicInfo.price_unit !== "0" ? state.basicInfo.price : "0",
        price_unit: state.basicInfo.price_unit,
        area: state.basicInfo.area,
        area_unit: "m2",
        address: state.basicInfo.address,

        description: state.description,

        main_direction: state.otherInfo.main_direction,
        nbr_bed_room: state.otherInfo.nbr_bed_room,
        nbr_rest_room: state.otherInfo.nbr_rest_room,
        nbr_floor: state.otherInfo.nbr_floor,
        front_street_width: state.otherInfo.front_street_width,
        behind_street_width: state.otherInfo.behind_street_width,

        longitude: state.mediaInfo.maps.longitude,
        latitude: state.mediaInfo.maps.latitude,

        contact_name: state.contactInfo.contact_name,
        contact_email: state.contactInfo.contact_email,
        contact_mobile: state.contactInfo.contact_mobile,
        contact_address: state.contactInfo.contact_address,

        province_id: state.basicInfo.province_id,
        district_id: state.basicInfo.district_id,
        ward_id: state.basicInfo.ward_id,
        street_id: state.basicInfo.street_id,
        project_id: state.basicInfo.project_id,

        product_type: state.basicInfo.product_type,
        product_category: state.basicInfo.product_category,
        post_type: state.post_type,
        start_date: state.scheduleInfo.start_date + " 00:00:00",
        end_date: state.scheduleInfo.end_date + " 00:00:00",

        thumbnails: state.mediaInfo.thumbnails,
        images: [],
        youtube: state.mediaInfo.youtube
      };

      // Xử lý thêm hình ảnh
      for (let i = 0; i < state.mediaInfo.files.length; i++) {
        let item = state.mediaInfo.files[i];

        submitPayload.images.push({
          filepath: item.filepath,
          type: item.type,
          sort_order: item.sort_order,
          uploaded_data: item.files
        });
      }

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        PostService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.title = basicInfo.title;

      state.basicInfo.product_type = basicInfo.product_type;
      state.basicInfo.product_category = basicInfo.product_category;

      state.basicInfo.province_id = basicInfo.province_id;
      state.basicInfo.district_id = basicInfo.district_id;
      state.basicInfo.ward_id = basicInfo.ward_id;
      state.basicInfo.street_id = basicInfo.street_id;
      state.basicInfo.project_id = basicInfo.project_id;
      state.basicInfo.address = basicInfo.address;

      state.basicInfo.price = basicInfo.price;
      state.basicInfo.price_unit = basicInfo.price_unit;
      state.basicInfo.area = basicInfo.area;
    },

    setDescription(state, description) {
      state.description = description;
    },

    setOtherInfo(state, otherInfo) {
      state.otherInfo.main_direction = otherInfo.main_direction;
      state.otherInfo.nbr_bed_room = otherInfo.nbr_bed_room;
      state.otherInfo.nbr_rest_room = otherInfo.nbr_rest_room;
      state.otherInfo.nbr_floor = otherInfo.nbr_floor;
      state.otherInfo.front_street_width = otherInfo.front_street_width;
      state.otherInfo.behind_street_width = otherInfo.behind_street_width;
    },

    setMapInfo(state, mapInfo) {
      state.mediaInfo.maps.longitude = mapInfo.longitude;
      state.mediaInfo.maps.latitude = mapInfo.latitude;
      state.mediaInfo.maps.zoom = mapInfo.zoom;
    },

    setContactInfo(state, contact) {
      state.contactInfo.contact_name = contact.contact_name;
      state.contactInfo.contact_address = contact.contact_address;
      state.contactInfo.contact_mobile = contact.contact_mobile;
      state.contactInfo.contact_email = contact.contact_email;
    },

    setPostType(state, type) {
      state.post_type = type;
    },

    setSchedule(state, schedule) {
      let a = moment(state.scheduleInfo.start_date);
      let b = moment(state.scheduleInfo.end_date);

      state.scheduleInfo.numberOfDay = b.diff(a, "days");

      state.scheduleInfo.start_date = schedule.start;
      state.scheduleInfo.end_date = schedule.end;
    },

    calculatePrice(state) {
      state.pricing.per_day = getPriceForPostType(state.post_type) * 1000;
      state.pricing.subtotal =
        state.pricing.per_day *
        (state.post_type !== 4 ? state.scheduleInfo.numberOfDay : 1);
      state.pricing.tax_amount = state.pricing.subtotal * 0.1;
      state.pricing.grand_total =
        state.pricing.subtotal + state.pricing.tax_amount;
    },

    // setThumbnails (state, url) {
    //   state.mediaInfo.thumbnails = url
    // },

    addImage(state, image) {
      image.sort_order = 0;
      state.mediaInfo.files.push(image);

      let thumbnailUrl =
        "http://127.0.0.1:8000/images/post/" + image["filename"];
      let rawUrl = "http://127.0.0.1:8000/images/post/" + image["filename"];

      for (let i = 0; i < image["files"].length; i++) {
        if (image["files"][i]["ranting"] === "xs") {
          thumbnailUrl =
            "http://127.0.0.1:8000/images/post/" +
            image["files"][i]["filename"];
          state.mediaInfo.thumbnails =
            "http://127.0.0.1:8000/images/post/" +
            image["files"][i]["filename"];
        }

        if (image["files"][i]["ranting"] === "o") {
          rawUrl =
            "http://127.0.0.1:8000/images/post/" +
            image["files"][i]["filename"];
        }
      }

      state.mediaInfo.preview.push({
        type: image.type,
        thumbnail: thumbnailUrl,
        raw: rawUrl
      });
    },

    removeImage(state, index) {
      state.mediaInfo.files.splice(index, 1);
      state.mediaInfo.preview.splice(index, 1);
    },

    setLinkYoutube(state, url) {
      state.mediaInfo.youtube = url;
    },

    reset(state) {
      state.basicInfo.province_id = 0;
      state.basicInfo.district_id = 0;
      state.basicInfo.project_id = 0;
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo,
    description: state => state.description,
    otherInfo: state => state.otherInfo,
    mediaInfo: state => state.mediaInfo,
    scheduleInfo: state => state.scheduleInfo,
    reset: state => state.reset
  }
};

export default PostAddModule;
