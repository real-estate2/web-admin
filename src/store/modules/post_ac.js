import AcService from "../../services/ac";
import _ from "lodash";

const PostAcModule = {
  namespaced: true,
  state: {
    isLoading: false,

    provinceOptions: [],
    districtOptions: [],
    wardOptions: [],
    streetOptions: [],
    projectOptions: [],

    polygon: [],
    latitude: null,
    longitude: null
  },
  actions: {
    loadProvinceData({ commit }) {
      commit("onLoadProvinceData");
      return new Promise((resolve, reject) => {
        AcService.getProvinces()
          .then(resp => {
            commit("onProcessingProvinceData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            commit("onProcessingProvinceDataFailure");
            reject(err);
          });
      });
    },

    changeProvince({ commit }, provinceId) {
      return new Promise((resolve, reject) => {
        AcService.getDistricts(provinceId)
          .then(resp => {
            commit("onProcessingDistrictData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    changeDistrict({ commit }, districtId) {
      return new Promise((resolve, reject) => {
        AcService.getDistrictBundle(districtId)
          .then(resp => {
            commit("onProcessingDistrictBundleData", resp.data.data);

            resolve(resp);
          })
          .catch(err => {
            reject(err);
          });
      });
    },

    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    onLoadProvinceData(state) {
      state.isLoading = true;
    },
    onProcessingProvinceData(state, data) {
      state.isLoading = false;

      state.provinceOptions = [
        { value: 0, text: "--- Chọn tỉnh thành phố ---" }
      ];
      state.districtOptions = [{ value: 0, text: "--- Chọn quận huyện ---" }];
      state.wardOptions = [{ value: 0, text: "--- Chọn phường xã ---" }];
      state.streetOptions = [{ value: 0, text: "--- Chọn đường phố ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.provinceOptions.push({
          value: data[i]["province_id"],
          text: data[i]["name"]
        });
      }
    },
    onProcessingProvinceDataFailure(state) {
      state.isLoading = false;
    },

    onProcessingDistrictData(state, data) {
      state.districts = data;

      state.districtOptions = [{ value: 0, text: "--- Chọn quận huyện ---" }];
      state.wardOptions = [{ value: 0, text: "--- Chọn phường xã ---" }];
      state.streetOptions = [{ value: 0, text: "--- Chọn đường phố ---" }];

      for (let i = 0, l = data.length; i < l; i++) {
        state.districtOptions.push({
          value: data[i]["district_id"],
          text: data[i]["pre"] + " " + data[i]["name"]
        });
      }
    },

    onProcessingDistrictBundleData(state, data) {
      state.wardOptions = [{ value: 0, text: "--- Chọn phường xã ---" }];
      state.streetOptions = [{ value: 0, text: "--- Chọn đường phố ---" }];
      state.projectOptions = [{ value: 0, text: "Không" }];

      if (data.wards) {
        let wards = _.orderBy(data.wards, ["name"], ["asc"]);
        for (let i = 0, l = wards.length; i < l; i++) {
          state.wardOptions.push({
            value: wards[i]["ward_id"],
            text: wards[i]["name"]
          });
        }
      }

      if (data.streets) {
        let streets = _.orderBy(data.streets, ["name"], ["asc"]);
        for (let i = 0, l = streets.length; i < l; i++) {
          state.streetOptions.push({
            value: streets[i]["street_id"],
            text: streets[i]["name"]
          });
        }
      }

      if (data.projects) {
        let projects = _.orderBy(data.projects, ["name"], ["asc"]);
        for (let i = 0, l = projects.length; i < l; i++) {
          state.projectOptions.push({
            value: projects[i]["id"],
            text: projects[i]["name"]
          });
        }
      }
    }
  }
};

export default PostAcModule;
