import CategoryService from "../../services/category";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    id: "",
    district_id: "",
    name: "",
    description: "",
    thumbnails: ""
  }
};

const CategoryAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    // /**
    //  * Thêm hình ảnh đại diện cho bài đăng
    //  *
    //  * @param commit
    //  * @param url
    //  */
    // setThumbnails ({ commit }, url) {
    //   commit('setThumbnails', url)
    // },

    /**
     * Thêm hình ảnh vào bài đăng nháp
     *
     * @param commit
     * @param image
     */
    addImage({ commit }, image) {
      commit("addImage", image);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        district_id: state.basicInfo.district_id,
        name: state.basicInfo.name,
        description: state.basicInfo.description,
        thumbnails: state.basicInfo.thumbnails
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        CategoryService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.district_id = basicInfo.district_id;
      state.basicInfo.name = basicInfo.name;
      state.basicInfo.description = basicInfo.description;
      state.basicInfo.thumbnails = basicInfo.thumbnails;
    },

    addImage(state, image) {
      state.basicInfo.thumbnails = image;
    },

    reset(state) {
      state.basicInfo.name = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default CategoryAddModule;
