import NewsService from "../../services/news";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    id: "",
    title: "",
    summary: "",
    content: "",
    content_type: "",
    category_id: "",
    thumbnails: "",
    source: "",
    start_date: "",
    end_date: ""
  }
};

const NewsAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    // /**
    //  * Thêm hình ảnh đại diện cho bài đăng
    //  *
    //  * @param commit
    //  * @param url
    //  */
    // setThumbnails ({ commit }, url) {
    //   commit('setThumbnails', url)
    // },

    /**
     * Thêm hình ảnh vào bài đăng nháp
     *
     * @param commit
     * @param image
     */
    addImage({ commit }, image) {
      commit("addImage", image);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        title: state.basicInfo.title,
        summary: state.basicInfo.summary,
        content: state.basicInfo.content,
        content_type: state.basicInfo.content_type,
        category_id: state.basicInfo.category_id,
        thumbnails: state.basicInfo.thumbnails,
        source: state.basicInfo.source,
        start_date: state.basicInfo.start_date,
        end_date: state.basicInfo.end_date
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        NewsService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.title = basicInfo.title;
      state.basicInfo.summary = basicInfo.summary;
      state.basicInfo.content = basicInfo.content;
      state.basicInfo.content_type = basicInfo.content_type;
      state.basicInfo.category_id = basicInfo.category_id;
      state.basicInfo.thumbnails = basicInfo.thumbnails;
      state.basicInfo.source = basicInfo.source;
      state.basicInfo.start_date = basicInfo.start_date;
      state.basicInfo.end_date = basicInfo.end_date;
    },

    // setThumbnails (state, url) {
    //   state.basicInfo.thumbnails = url
    // },

    addImage(state, image) {
      console.log("<========");
      console.log(image);
      state.basicInfo.thumbnails = image;
      console.log(state.basicInfo.thumbnails);
    },

    reset(state) {
      state.basicInfo.name = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default NewsAddModule;
