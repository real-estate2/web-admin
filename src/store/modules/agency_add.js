import AgencyService from "../../services/agency.js";

const defaultState = {
  isSubmitting: false,

  basicInfo: {
    id: "",
    contact_name: "",
    thumbnails: "",
    position: "",
    contact_mobile: "",
    contact_email: "",
    company: "",
    experience: "",
    introduce: ""
  }
};

const AgencyAddModule = {
  namespaced: true,
  state: JSON.parse(JSON.stringify(defaultState)),
  actions: {
    /**
     * Cập nhật thông tin cơ bản
     *
     * @param commit
     * @param basicInfo
     */
    setBasicInfo({ commit }, basicInfo) {
      commit("setBasicInfo", basicInfo);
    },

    // /**
    //  * Thêm hình ảnh đại diện cho bài đăng
    //  *
    //  * @param commit
    //  * @param url
    //  */
    // setThumbnails ({ commit }, url) {
    //   commit('setThumbnails', url)
    // },

    /**
     * Thêm hình ảnh vào bài đăng nháp
     *
     * @param commit
     * @param image
     */
    addImage({ commit }, image) {
      commit("addImage", image);
    },

    /**
     * Đăng danh mục
     *
     * @param commit
     * @param state
     */
    submit({ commit, state }) {
      let submitPayload = {
        contact_name: state.basicInfo.contact_name,
        thumbnails: state.basicInfo.thumbnails,
        position: state.basicInfo.position,
        contact_mobile: state.basicInfo.contact_mobile,
        contact_email: state.basicInfo.contact_email,
        company: state.basicInfo.company,
        experience: state.basicInfo.experience,
        introduce: state.basicInfo.introduce
      };

      commit("onSubmitting");
      return new Promise((resolve, reject) => {
        AgencyService.submit(submitPayload)
          .then(function(response) {
            commit("onProcessingSubmit", response.data.data);

            resolve(response);
          })
          .catch(function(error) {
            commit("onSubmitDataFailure");
            reject(error);
          });
      });
    },

    /**
     * Xóa trắng form nhập
     *
     * @param commit
     */
    reset({ commit }) {
      commit("reset");
    }
  },
  mutations: {
    // Quá trình submit chạy
    onSubmitting(state) {
      state.isSubmitting = true;
    },
    onProcessingSubmit(state) {
      state.isSubmitting = false;
    },
    onSubmitDataFailure(state) {
      state.isSubmitting = false;
    },

    setBasicInfo(state, basicInfo) {
      state.basicInfo.contact_name = basicInfo.contact_name;
      state.basicInfo.thumbnails = basicInfo.thumbnails;
      state.basicInfo.position = basicInfo.position;
      state.basicInfo.contact_mobile = basicInfo.contact_mobile;
      state.basicInfo.contact_email = basicInfo.contact_email;
      state.basicInfo.company = basicInfo.company;
      state.basicInfo.experience = basicInfo.experience;
      state.basicInfo.introduce = basicInfo.introduce;
    },

    // setThumbnails (state, url) {
    //   state.basicInfo.thumbnails = url
    // },

    addImage(state, image) {
      console.log("<========");
      console.log(image);
      state.basicInfo.thumbnails = image;
      console.log(state.basicInfo.thumbnails);
    },

    reset(state) {
      state.basicInfo.contact_name = "";
    }
  },
  getters: {
    // Status
    isSubmitting: state => state.isSubmitting,
    // Form data
    basicInfo: state => state.basicInfo
  }
};

export default AgencyAddModule;
